package INF102.lab3.numberGuesser;


public class MyGeniusGuesser implements IGuesser {

	@Override
    public int findNumber(RandomNumber number) {

        int guessUpperBound = number.getUpperbound(); 
        int guessLowerBound = number.getLowerbound();
        int answer;
        int guess;

        do {
            guess = (guessUpperBound-guessLowerBound)/2 + guessLowerBound;
            answer = number.guess(guess);
            if (answer > 0) 
                guessUpperBound = guess;
            if (answer < 0)
                guessLowerBound = guess;
        } while (answer != 0);
    
        return guess;

    }

}
