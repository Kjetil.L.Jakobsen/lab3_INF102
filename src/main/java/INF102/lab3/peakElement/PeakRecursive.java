package INF102.lab3.peakElement;

import java.util.List;
import java.lang.Math;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {

        int potensialPeak = numbers.remove(0);
        if (!numbers.isEmpty())
            if (numbers.get(0) > potensialPeak)
                return Math.max(numbers.get(0), peakElement(numbers));
            else {
                return potensialPeak;
            }
        else {
            return potensialPeak;
        }
        
    }

}

/* 
 * 
*/
