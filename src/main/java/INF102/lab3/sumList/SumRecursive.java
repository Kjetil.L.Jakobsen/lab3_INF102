package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

    @Override
    public long sum(List<Long> list) {
        
        long sumOfList = 0;
        if (!list.isEmpty())
            sumOfList = list.remove(list.size()-1);
        if (list.isEmpty())
            return sumOfList;
        return sumOfList += sum(list);

    }
}
